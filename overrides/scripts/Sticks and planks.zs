recipes.addShaped(<minecraft:stick> * 2, [[null, <ore:craftingToolSaw>, null], [null, <ore:plankWood>, null]]);
recipes.addShapeless(<Natura:planks>*2, [<Natura:tree>]);
recipes.addShapeless(<Natura:planks:1>*2, [<Natura:tree:1>]);
recipes.addShapeless(<Natura:planks:2>*2, [<Natura:tree:2>]);
recipes.addShapeless(<Natura:planks:3>*2, [<Natura:tree:5>]);
recipes.addShapeless(<Natura:planks:11>*2, [<Natura:Dark Tree>]);
recipes.addShapeless(<Natura:planks:12>*2, [<Natura:Dark Tree:1>]);
recipes.addShapeless(<Natura:planks:10>*2, [<Natura:willow>]);
recipes.addShapeless(<Natura:planks:8>*2, [<Natura:Rare Tree:2>]);
recipes.addShapeless(<Natura:planks:4>*2, [<Natura:bloodwood>]);
recipes.addShapeless(<Natura:planks:6>*2, [<Natura:Rare Tree>]);
recipes.addShapeless(<Natura:planks:7>*2, [<Natura:Rare Tree:1>]);
recipes.addShapeless(<Natura:planks:8>*2, [<Natura:Rare Tree:2>]);


