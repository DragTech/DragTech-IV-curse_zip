/* Recipe removals
Created by Kupferdrache
File version: 0.8.6 */

// import mod to remove repices from

#import mods.buildcraft.???;

// define static variables

#val plankin = <minecraft:orePlank>;
#val plankout =<minecraft:plank:*>;

// define editable variables

#var wood = <minecraft:oreWood>

// removes

recipes.remove(<minecraft:stick>);
recipes.remove(<minecraft:wooden_slab>);
recipes.remove(<appliedenergistics2:item.ItemCrystalSeed>);
recipes.remove(<appliedenergistics2:item.ItemCrystalSeed:600>);
recipes.remove(<appliedenergistics2:item.ItemCrystalSeed:1200>);recipes.removeShaped(<BuildCraft|Core:diamondGearItem>, [[null, <ore:gemDiamond>, null], [<ore:gemDiamond>, <ore:thermalexpansion:machineGold>, <ore:gemDiamond>], [null, <minecraft:diamond>, null]]);
recipes.removeShaped(<BuildCraft|Core:goldGearItem>, [[null, <ore:ingotGold>, null], [<ore:ingotGold>, <ore:thermalexpansion:machineIron>, <ore:ingotGold>], [null, <minecraft:gold_ingot>, null]]);
recipes.removeShaped(<BuildCraft|Core:ironGearItem>, [[null, <ore:ingotAnyIron>, null], [<ore:ingotAnyIron>, <ore:gearStone>, <ore:ingotAnyIron>], [null, <minecraft:iron_ingot>, null]]);
recipes.removeShaped(<BuildCraft|Core:stoneGearItem>, [[null, <ore:stoneCobble>, null], [<ore:stoneCobble>, <ore:gearWood>, <ore:stoneCobble>], [null, <ore:stoneCobble>, null]]);
recipes.removeShaped(<BuildCraft|Core:woodenGearItem>, [[null, <ore:stickWood>, null], [<ore:stickWood>, null, <ore:stickWood>], [null, <ore:stickWood>, null]]);
recipes.remove(<BuildCraft|Builders:machineBlock>);
recipes.remove(<BuildCraft|Factory:pumpBlock>);
recipes.remove(<BuildCraft|Factory:miningWellBlock>);
recipes.remove(<BigReactors:BRReactorPart:1>);
recipes.remove(<BigReactors:BRDevice>);
recipes.remove(<BigReactors:BRTurbinePart:1>);
recipes.remove(<Botania:pestleAndMortar>);
recipes.remove(<chisel:obsidianChisel>);
recipes.remove(<chisel:diamondChisel>);
recipes.remove(<DimensionalAnchors:chunkloader>);
recipes.remove(<ExtraUtilities:watering_can:1>);
recipes.remove(<ExtraUtilities:generator>);
recipes.remove(<ExtraUtilities:generator:1>);
recipes.remove(<ExtraUtilities:generator:2>);
recipes.remove(<ExtraUtilities:generator:3>);
recipes.remove(<ExtraUtilities:generator:4>);
recipes.remove(<ExtraUtilities:generator:5>);
recipes.remove(<ExtraUtilities:generator:6>);
recipes.remove(<ExtraUtilities:generator:7>);
recipes.remove(<ExtraUtilities:generator:8>);
recipes.remove(<ExtraUtilities:generator:9>);
recipes.remove(<ExtraUtilities:generator:10>);
recipes.remove(<ExtraUtilities:generator:11>);
recipes.remove(<ExtraUtilities:enderThermicPump>);
recipes.remove(<ExtraUtilities:enderQuarry>);
recipes.remove(<Forestry:sturdyMachine>);
recipes.remove(<Forestry:factory:2>);
recipes.remove(<Forestry:factory:1>);
recipes.remove(<Forestry:engine:3>);
recipes.remove(<Forestry:factory:5>);
recipes.remove(<Forestry:gearTin>);
recipes.remove(<Forestry:gearCopper>;
recipes.remove(<Forestry:gearBronze>);
recipes.remove(<ForgeMicroblock:stoneRod>);
recipes.remove(<ForgeMicroblock:sawStone>);
recipes.remove(<ForgeMicroblock:sawIron>);
recipes.remove(<ForgeMicroblock:sawDiamond>);
recipes.remove(<ProjRed|Exploration:projectred.exploration.sawgold>);
recipes.remove(<ProjRed|Exploration:projectred.exploration.sawruby>);
recipes.remove(<ProjRed|Exploration:projectred.exploration.sawsapphire>);
recipes.remove(<ProjRed|Exploration:projectred.exploration.sawperidot>);
recipes.remove(<GalacticraftMars:item.carbonFragments>);
recipes.remove(<GalacticraftMars:item.carbonFragments>);
recipes.remove(<ganyssurface.stick:*>);
recipes.remove(<IC2:itemToolDDrill:*>);
recipes.remove(<IC2:itemCable:1>);
recipes.remove(<IC2:itemCable:5>);
recipes.remove(<IC2:itemCable:2>);
recipes.remove(<IC2:itemCable:10>);
recipes.remove(<Natura:plankSlab1>);
recipes.remove(<Natura:planks:*>);
recipes.remove(<Natura:natura.stick:*>);
recipes.remove(<harvestcraft:market>);
recipes.remove(<harvestcraft:mortarandpestleItem>);
recipes.remove(<harvestcraft:cuttingboardItem>);
recipes.remove(<harvestcraft:juicerItem>);
recipes.remove(<harvestcraft:bakewareItem>);
recipes.remove(<harvestcraft:skilletItem>);
recipes.remove(<harvestcraft:saucepanItem:*>);
recipes.remove(<harvestcraft:potItem>);
recipes.remove(<Railcraft:machine.alpha:8>);
recipes.remove(<ThermalExpansion:Frame:3>);
recipes.remove(<ThermalExpansion:Frame:2>);
recipes.remove(<ThermalExpansion:Frame:1>);
recipes.remove(<ThermalExpansion:Frame>);
recipes.remove(<ThermalExpansion:Cell:4>);
recipes.remove(<ThermalExpansion:Cell:4>);
recipes.remove(<ThermalExpansion:Cell:3>);
recipes.remove(<ThermalExpansion:Cell:2>);
recipes.remove(<ThermalExpansion:Cell:2>);
recipes.remove(<ThermalExpansion:Frame:5>);
recipes.remove(<ThermalExpansion:Cell:2>);
recipes.remove(<ThermalExpansion:Cell:1>);
recipes.remove(<ThermalExpansion:material:3>);
recipes.remove(<ThermalExpansion:material:2>);
recipes.remove(<ThermalExpansion:material:1>);
recipes.remove(<ThermalExpansion:Frame:4>);
recipes.removeS(<ThermalExpansion:Frame:10>);
recipes.removeS(<ThermalExpansion:Frame:8>);
recipes.removeS(<ThermalExpansion:Frame:6>);
recipes.remove(<ThermalExpansion:Machine:1>);
recipes.remove(<ThermalExpansion:Machine:2>);
recipes.remove(<ThermalExpansion:Machine:3>);
recipes.remove(<ThermalExpansion:Machine:4>);
recipes.remove(<ThermalExpansion:Machine:5>);
recipes.remove(<ThermalExpansion:Machine:8>);
recipes.remove(<ThermalExpansion:Machine:6>);
recipes.remove(<ThermalExpansion:Machine:7>);
recipes.remove(<ThermalExpansion:Machine:11>);
recipes.remove(<ThermalFoundation:material:139>);
recipes.remove(<ThermalFoundation:material:140>);
recipes.remove(<ThermalFoundation:material:138>);
recipes.remove(<ThermalFoundation:material:137>);
recipes.remove(<ThermalFoundation:material:128>);
recipes.remove(<ThermalFoundation:material:129>);
recipes.remove(<ThermalFoundation:material:134>);
recipes.remove(<ThermalFoundation:material:12>);
recipes.remove(<ThermalFoundation:material:13>);
recipes.remove(<ThermalFoundation:material:132>);
recipes.remove(<ThermalFoundation:material:133>);
recipes.remove(<ThermalFoundation:material:136>);
recipes.remove(<ThermalFoundation:material:131>);
recipes.remove(<ThermalFoundation:material:135>);
recipes.remove(<ThermalFoundation:material:130>);
recipes.remove(<ThermalExpansion:Dynamo>);
recipes.remove(<ThermalExpansion:Dynamo:1>);
recipes.remove(<ThermalExpansion:Dynamo:3>);
recipes.remove(<ThermalExpansion:Dynamo:4>);
recipes.remove(<ThermalExpansion:Dynamo:2>);
recipes.remove(<TConstruct:toolRod:*>);
recipes.remove(<WaterPower:cptItemMeterial:7>);
recipes.remove(<WaterPower:cptItemMeterial:107>);
recipes.remove(<WaterPower:cptItemMeterial:207>);
recipes.remove(<WaterPower:cptItemMeterial:307>);
recipes.remove(<WaterPower:cptItemMeterial:407>);
recipes.remove(<WaterPower:cptItemMeterial:507>);
recipes.remove(<WaterPower:cptItemMeterial:607>);
recipes.remove(<WaterPower:cptItemMeterial:707>);
recipes.remove(<WaterPower:cptItemMeterial:807>);
recipes.remove(<WaterPower:cptItemMeterial:907>);
recipes.remove(<WaterPower:cptItemMeterial:1007>);
